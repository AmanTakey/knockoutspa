const path = require('path');

module.exports = {
  mode:"development",
    entry: {
      main: path.resolve(__dirname, './src/main.js'),
    },
    output: {
      filename: '[name].js',
    },
    resolve: {
      modules: [
          'node_modules',
      ],
      extensions: ['.js', '.css'], // File types,
      alias: {
      }
  }
};