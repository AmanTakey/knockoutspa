export class Router {
    constructor(config) {
        config.routes.forEach(route => {
            crossroads.addRoute(route.url, () => {
                $('#routeContent').load(route.params.page);
            });
        });
        
        crossroads.bypassed.add(function(request) {
            console.error(request + ' seems to be a dead end...');
        });

        //Listen to hash changes
        window.addEventListener("hashchange", function() {
            var route = '/';
            var hash = window.location.hash;
            if (hash.length > 0) {
                route = hash.split('#').pop();
            }
            crossroads.parse(route);
        });
    }
}