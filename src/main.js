import { Router } from "./router";

// Define the routes
require("./modules/employee-manager.module.js");

var routerInstance = new Router({
    routes: [
        { url: 'address',          params: { page: 'views/address-manager.html' } },
        { url: 'employee',     params: { page: 'views/employee-manager.html' } }
    ]
});

window.dispatchEvent(new CustomEvent("hashchange"));
window.location.hash="employee";
